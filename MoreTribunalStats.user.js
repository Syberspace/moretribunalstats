// ==UserScript==
// @name        MoreTribunalStats
// @namespace   Tribunal
// @description Provides additional stats in the LoL Tribunal
// @include     http://*.leagueoflegends.com/tribunal/*justice-review/*
// @version     1.2
// @author	Syberspace
// @url		http://blog.anvor.at/
// ==/UserScript==

//----config your stats here----

var stat = new Array(5);
stat[1] = 'Rating per case';
stat[2] = '100% Rating';
stat[3] = 'Toxic Days per case';
stat[4] = 'Percent Permabanned';

var desc = new Array(5);
desc[1] = '';
desc[2] = 'The rating you would have if all your cases were correct';
desc[3] = '';
desc[4] = '';


/*
* Which variables can be used?
*
* accuracy	- your current voting accuracy (sans the percent sign)
* casesCorrect	- the number of cases you gave the correct vote on
* casesPending	- the number of cases you voted on, but have not been solved yet
* casesReviewed	- the number of total cases solved (excluding pending cases)
* days		- the number of toxic days prevented
* permabanned	- the number of players you have helped to ban permanently
* ranking	- your current tribunal ranking
* rating  	- represents your current justice rating (aka the big fat number in the middle)
*
*/
function calcStat1()
{
    return Math.round(rating/casesReviewed*100)/100;
}

function calcStat2()
{
	max_rating = 1200;
	for(i = 0; i <= casesReviewed; i++)
	{
		max_rating += Math.round(10 * Math.sqrt(i));
	}
	return max_rating;
}

function calcStat3()
{
    return Math.round(days/casesCorrect*100)/100;
}

function calcStat4()
{
    return Math.round(10000*permabanned/casesCorrect)/100+'%';
}

//----end config----


//preparing the new stats panel for easier access and html validity
function initBeautifyFF()
{
    var stats = document.getElementById('justice-review').children[1].cloneNode(true);
    var titles = stats.children[0].children[0].children[0];
    var values = stats.children[0].children[0].children[1];
	
    beautify(titles, values);
    document.getElementById('justice-review').insertBefore(stats, document.getElementById('justice-review').children[2]);
}

function initBeautifyChrome()
{
	var stats = document.getElementById('justice-review').children[1].cloneNode(true);
	var titles = stats.childNodes[1].childNodes[1].children[0];
    var values = stats.childNodes[1].childNodes[1].children[1];
	
	beautify(titles, values);
	document.getElementById('justice-review').insertBefore(stats, document.getElementById('justice-review').childNodes[5]);
}

function beautify(titles, values)
{
	for(i=0;i<4;i++)
	{
		titles.children[i].innerHTML = stat[i+1];
		values.children[i].id = 'cstat'+(i+1);
		titles.children[i].title= desc[i+1];
		values.children[i].title= desc[i+1];
	}
}



if(navigator.userAgent.toLowerCase().indexOf('firefox') !== -1)
{
	initBeautifyFF();
}
else
{
	initBeautifyChrome();
}

//gathering the provided data
var accuracy = document.getElementById('accuracy').children[0].innerHTML.replace("%","");
var casesCorrect = document.getElementById('accuracy-fill').innerHTML;
var casesPending = document.getElementById('stats').children[0].children[2].children[0].innerHTML;
var casesReviewed = document.getElementById('stats').children[0].children[1].innerHTML;
var days = document.getElementById('toxic-days-prevented-fill').innerHTML;
var permabanned = document.getElementById('players-permabanned-fill').innerHTML;
var ranking = document.getElementById('stats').children[2].children[1].innerHTML;
var rating = document.getElementById('justice-rating').children[1].innerHTML;

ranking = isNaN(ranking) ? 0 : ranking;

//calculating each stat according to the user's definition
try
{
    document.getElementById('cstat1').innerHTML = calcStat1();
    document.getElementById('cstat2').innerHTML = calcStat2();
    document.getElementById('cstat3').innerHTML = calcStat3();
    document.getElementById('cstat4').innerHTML = calcStat4();
}
catch(e)
{
    alert("MoreTribunalStats encountered an error: \r\n" + e);
}
